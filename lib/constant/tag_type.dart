class TagType{
  static const String kSignerName='SignerName';
  static const String kTimeStamp='TimeStamp';
  static const String kSignerText='SignerText';
  static const String kReason='Reason';
  static const String kSignatureContract='SignatureContract';
  static const String kInitials='Initials';
  static const String kSignerTitle='SignerTitle';
  static const String kAttachment='Attachment';
  static const String kSignedAttachment='SignedAttachment';
  static const String kCustomText='CustomText';
  static const String kCustomTextArea='CustomTextArea';
  static const String kCheckBox='CheckBox';
  static const String kImage='Image';
  static const String kCalender='Calender';
  static const String kSignature21CFR='Signature21CFR';
  static const String kSignature='Signature';
  static const String kSignHere='SignHere';
  static const String kUserUUID='UserUUID';
}
